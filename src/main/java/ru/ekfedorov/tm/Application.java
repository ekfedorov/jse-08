package ru.ekfedorov.tm;

import ru.ekfedorov.tm.api.ICommandRepository;
import ru.ekfedorov.tm.constant.ArgumentConst;
import ru.ekfedorov.tm.constant.TerminalConst;
import ru.ekfedorov.tm.model.Command;
import ru.ekfedorov.tm.repository.CommandRepository;
import ru.ekfedorov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        displayWelcome();
        if (run(args)) exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP: displayHelp(); break;
            case ArgumentConst.ARG_VERSION: displayVersion(); break;
            case ArgumentConst.ARG_ABOUT: displayAbout(); break;
            case ArgumentConst.ARG_INFO: showSystemInfo(); break;
            default: showIncorrectArgument();
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP: displayHelp(); break;
            case TerminalConst.CMD_VERSION: displayVersion(); break;
            case TerminalConst.CMD_ABOUT: displayAbout(); break;
            case TerminalConst.CMD_EXIT: exit(); break;
            case TerminalConst.CMD_INFO: showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: showArguments(); break;
            default: showIncorrectCommand();
        }
    }

    private static void showIncorrectCommand() {
        System.out.println("Error! Command not found...\n");
    }

    private static void showIncorrectArgument() {
        System.out.println("Error! Argument not found...");
    }

    private static boolean run(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        parseArg(param);
        return true;
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
        System.out.println();
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.3.0");
        System.out.println();
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Evgeniy Fedorov");
        System.out.println("ekfedorov@tsconsulting.com");
        System.out.println();
    }

    private static void exit() {
        System.exit(0);
    }

    public static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name != null) System.out.println(name);
        }
        System.out.println();
    }

    public static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            final String arg = command.getArg();
            if (arg != null) System.out.println(arg);
        }
        System.out.println();
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        final String processorMsg = "Available processors: " + processors;
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryMsg = "Free memory: " + NumberUtil.format(freeMemory);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.format(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final String maxMemoryMsg = "Maximum memory: " + maxMemoryValue;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryMsg = "Total memory available to JVM: " + NumberUtil.format(totalMemory);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryMsg = "Used memory by JVM: " + NumberUtil.format(usedMemory);
        System.out.println(processorMsg);
        System.out.println(freeMemoryMsg);
        System.out.println(maxMemoryMsg);
        System.out.println(totalMemoryMsg);
        System.out.println(usedMemoryMsg);
        System.out.println();
    }

}
