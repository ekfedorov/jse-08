package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
